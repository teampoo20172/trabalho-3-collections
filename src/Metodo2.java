import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Metodo2 {
	/*
	 * Metodo 2 Dadas duas palavras quaisquer, quais são os arquivos que contêm
	 * apenas uma das palavras (mas não as duas)?
	 */
	public static void contemUmaMasNaoOutra(String fileName, String contem, String contem2) throws IOException {
		boolean palavra1 = false;
		boolean palavra2 = false;
		Path path1 = Paths.get(fileName);
		try (BufferedReader reader = new BufferedReader(Files.newBufferedReader(path1, Charset.forName("utf8")))) {
			String line;
			while ((line = reader.readLine()) != null) {
				line = line.toLowerCase().replaceAll("[^a-zA-Záéíóúçãõà-]", " ");
				String[] palavras = line.split(" ");
				for (String pal : palavras) {
					//System.out.println(pal);
					if(pal.equalsIgnoreCase(contem)) palavra1 = true;
					if(pal.equalsIgnoreCase(contem2)) palavra2 = true;
					if(palavra1 && palavra2) break;
				}
			}
			if (palavra1 && !palavra2) {
				System.out.println("Contém apenas " + contem);
			} else if (!palavra1 && palavra2) {
				System.out.println("Contém apenas " + contem2);
			} else if (palavra1 && palavra2) {
				System.out.println("Contém ambas as palavras");
			} else if (!palavra1 && !palavra2) {
				System.out.format("Não contém nenhuma das palavras. \n");
			}
		}
	}
}
