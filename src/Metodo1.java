import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Metodo1 {
	public static boolean listasComPalavra(String file, String palavra) throws IOException {
		Path path1 = Paths.get(file);
		try (BufferedReader reader = new BufferedReader(Files.newBufferedReader(path1, Charset.forName("utf8")))) {
			String line;
			while ((line = reader.readLine()) != null) {
				line = line.toLowerCase().replaceAll("[^a-zA-Záéíóúçãõà-]", " ");
				String[] palavras = line.split(" ");
				for (String pal : palavras) {
					if (pal.equals(palavra)) {
						System.out.println("O texto cont�m a palavra");
						return true;
					}
				}
				//System.out.format("\n");

			}
		}
		System.out.println("O texto n�o cont�m a palavra");
		return false;
	}
}
