import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class Metodo3 {
	/*
	 * Metodo 3
	 */
	public static void contagemPalavras(String fileName) throws IOException {
		Map<String, Integer> countWords = new HashMap<String, Integer>();
		Path path1 = Paths.get(fileName);
		try (BufferedReader reader = new BufferedReader(Files.newBufferedReader(path1, Charset.forName("utf8")))) {
			String line;
			while ((line = reader.readLine()) != null) {
				line = line.toLowerCase().replaceAll("[^a-zA-Záéíóúçãõà-]", " ");
				String[] palavras = line.split(" ");
				for (String pal : palavras) {
					Integer count = countWords.get(pal);
					if(count != null) {
						countWords.put(pal, count+1);
					} else {
						countWords.put(pal, 1);
					}
				}
			}
		}
		//organiza e printa
		countWords.entrySet().stream()
        .sorted(Map.Entry.<String, Integer>comparingByValue().reversed()) 
        .forEach(System.out::println);
	}	 
	public static void contagemPalavras(String[] fileNames) throws IOException {
		Map<String, Integer> countWords = new HashMap<String, Integer>();
		for(int i = 0; i < fileNames.length; i++) {
			Path path1 = Paths.get(fileNames[i]);
			try (BufferedReader reader = new BufferedReader(Files.newBufferedReader(path1, Charset.forName("utf8")))) {
				String line;
				while ((line = reader.readLine()) != null) {
					line = line.toLowerCase().replaceAll("[^a-zA-Záéíóúçãõà-]", " ");
					String[] palavras = line.split(" ");
					for (String pal : palavras) {
						Integer count = countWords.get(pal);
						if(count != null) {
							countWords.put(pal, count+1);
						} else {
							countWords.put(pal, 1);
						}
					}
				}
				reader.close();
			}
		}
		countWords.entrySet().stream()
        .sorted(Map.Entry.<String, Integer>comparingByValue().reversed()) 
        .forEach(System.out::println);
	}	 
}
